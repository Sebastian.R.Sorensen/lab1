package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() { 
        // End loop
        int ask = 0;

        while (ask < 1) {
            System.out.println("Let's play round " + roundCounter);
            String userinput = "";
            while (userinput.equals("")){
                userinput = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
                if (userinput.equals("rock") || userinput.equals("paper") || userinput.equals("scissors")){
                    break;}
                else{
                    System.out.println("I do not understand " + userinput + ". Could you try again?");
                    userinput = "";
                    continue;}
            }
            // Random data choice
            String datachoice = rpsChoices.get(new Random().nextInt(rpsChoices.size()));

            // User wins
            if ((userinput.equals("rock")) && (datachoice.equals("scissors" ))){
                System.out.println("Human chose " + userinput + ", computer chose " + datachoice + ". Human wins!");
                humanScore += 1;}
            else if ((userinput.equals("paper")) && (datachoice.equals("rock" ))){
                System.out.println("Human chose " + userinput + ", computer chose " + datachoice + ". Human wins!");
                humanScore += 1;}           
            else if ((userinput.equals("scissors")) && (datachoice.equals("paper" ))){
                System.out.println("Human chose " + userinput + ", computer chose " + datachoice + ". Human wins!");
                humanScore += 1;}

            // Equal
            else if (userinput.equals(datachoice)){
                System.out.println("Human chose " + userinput + ", computer chose " + datachoice + ". It's a tie!");}

            // Data Wins
            else {
                System.out.println("Human chose " + userinput + ", computer chose " + datachoice + ". Computer wins!");
                computerScore += 1;}

            // Show score
            System.out.println("Score: human " + humanScore + ", computer "  + computerScore);
            roundCounter += 1;
            
            String check = "";
            while (check.equals("")){
                check = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
                if (check.equals("y")){
                    continue;}
                else if (check.equals("n")) {
                    ask += 1;
                    System.out.println("Bye bye :)");}
                else {
                    System.out.println("I do not understand " + check + ". Could you try again?");
                    check = "";
                    continue; }
            }
        }
        // TODO: Implement Rock Paper Scissors
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
